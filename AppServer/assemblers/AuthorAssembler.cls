 
/*------------------------------------------------------------------------
   File        : AuthorController
   Purpose     : To process author data
   Syntax      : 
   Description : Author Controller for book
   Author(s)   : vinodrockson
   Created     : Mon Jul 22 23:31:08 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.AuthorAssembler: 

    /*------------------------------------------------------------------------------
     Purpose: Process the author name(s) of book
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER processAuthor( INPUT iAuthor AS CHARACTER ):
        DEFINE VARIABLE i        AS INTEGER   NO-UNDO.
        DEFINE VARIABLE authorID AS INTEGER   NO-UNDO.
        DEFINE VARIABLE RESULT   AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iAuthor):
            authorID = THIS-OBJECT:createAuthor(TRIM(ENTRY(i,iAuthor))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + STRING(authorID).    
        END.      
        
        RETURN RESULT.        
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To create the author in DB
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER createAuthor(INPUT authorName AS CHARACTER):
        DEFINE VARIABLE RESULT AS INTEGER NO-UNDO.
        DEFINE VARIABLE newID  AS INTEGER NO-UNDO.
        
        FIND FIRST author WHERE author.name = authorName NO-LOCK NO-ERROR.
        
        IF AVAILABLE author THEN
            RESULT = author.id.
        ELSE 
        DO:
            newID = NEXT-VALUE (AUTHOR_ID_SEQ,lms).
            
            CREATE author. 
            ASSIGN 
                author.id   = newID
                author.name = authorName
                .
            RESULT = newID.
        END. 
        
        RETURN RESULT.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: Parse author Id(s) to Name
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER parseAuthorId( INPUT iAuthorId AS CHARACTER ):
        
        DEFINE VARIABLE i          AS INTEGER   NO-UNDO.
        DEFINE VARIABLE authorName AS CHARACTER NO-UNDO.
        DEFINE VARIABLE RESULT     AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iAuthorId):
            authorName = THIS-OBJECT:getAuthorName(INTEGER(TRIM(ENTRY(i,iAuthorId)))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + authorName.    
        END.      
        
        RETURN RESULT.

    END METHOD.    

    /*------------------------------------------------------------------------------
     Purpose: To get author name by author Id
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER getAuthorName( INPUT authorId AS INTEGER  ):
        
        DEFINE VARIABLE result AS CHARACTER NO-UNDO INITIAL "".
        
        FIND FIRST author WHERE author.id = authorId NO-LOCK NO-ERROR.
        
        IF AVAILABLE author THEN
            RESULT = author.name.
      
        RETURN result.

    END METHOD.
    
    
END CLASS.