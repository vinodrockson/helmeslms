 
/*------------------------------------------------------------------------
   File        : BookAssembler
   Purpose     : To check and provide data
   Syntax      : 
   Description : Book assembler provides and process data    
   Author(s)   : vinodrockson
   Created     : Wed Jul 24 23:17:24 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING models.BookModel FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.BookAssembler: 

    /*------------------------------------------------------------------------------
     Purpose: To check if the books is active
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC BookModel getBookById( INPUT bookId AS INTEGER):

        DEFINE VARIABLE RESULT AS CLASS BookModel NO-UNDO.
        
        FIND FIRST book WHERE book.id EQ bookId NO-LOCK NO-ERROR.
        IF AVAILABLE book THEN 
            RESULT = NEW BookModel(book.book_title, book.isbn, book.published_date, book.is_active, book.registered_date, book.updated_date,
                book.author_id, book.publisher_id, book.category_id, book.inactive_reason_id, book.inactive_accused, book.language_id, book.price).
            
        RETURN RESULT.

    END METHOD.

END CLASS.