 
/*------------------------------------------------------------------------
   File        : CategoryController
   Purpose     : To process and store authors
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Mon Jul 22 22:39:32 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.CategoryAssembler: 

    /*------------------------------------------------------------------------------
     Purpose: Process the category name(s) of book
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER processCategory( INPUT iCategory AS CHARACTER ):
        DEFINE VARIABLE i          AS INTEGER   NO-UNDO.
        DEFINE VARIABLE categoryID AS INTEGER   NO-UNDO.
        DEFINE VARIABLE RESULT     AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iCategory):
            categoryID = THIS-OBJECT:createCategory(TRIM(ENTRY(i,iCategory))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + STRING(categoryID).    
        END.      
        
        RETURN RESULT.
        
    END METHOD.


    /*------------------------------------------------------------------------------
     Purpose: To create the category in DB
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER createCategory(INPUT categoryName AS CHARACTER):
        DEFINE VARIABLE RESULT AS INTEGER NO-UNDO.
        DEFINE VARIABLE newID  AS INTEGER NO-UNDO.
        
        FIND FIRST category WHERE category.name = categoryName NO-LOCK NO-ERROR.
        
        IF AVAILABLE category THEN
            RESULT = category.id.
        ELSE 
        DO:
            newID = NEXT-VALUE (CATEGORY_ID_SEQ,lms).
            
            CREATE category. 
            ASSIGN 
                category.id   = newID
                category.name = categoryName
                .
            RESULT = newID.
        END. 
        
        RETURN RESULT.
    END METHOD.
    
    /*------------------------------------------------------------------------------
    Purpose: Parse category Id(s) to Name
    Notes:
   ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER parseCategoryId( INPUT iCategoryId AS CHARACTER ):
        
        DEFINE VARIABLE i            AS INTEGER   NO-UNDO.
        DEFINE VARIABLE categoryName AS CHARACTER NO-UNDO.
        DEFINE VARIABLE RESULT       AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iCategoryId):
            categoryName = THIS-OBJECT:getCategoryName(INTEGER(TRIM(ENTRY(i,iCategoryId)))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + categoryName.    
        END.      
        
        RETURN RESULT.

    END METHOD.    

    /*------------------------------------------------------------------------------
     Purpose: To get category name by category Id
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER getCategoryName( INPUT categoryId AS INTEGER  ):
        
        DEFINE VARIABLE result AS CHARACTER NO-UNDO INITIAL "".
        
        FIND FIRST category WHERE category.id = categoryId NO-LOCK NO-ERROR.
        
        IF AVAILABLE category THEN
            RESULT = category.name.
      
        RETURN result.

    END METHOD.
END CLASS.