 
/*------------------------------------------------------------------------
   File        : ClientAssembler
   Purpose     : To provide functionality for client
   Syntax      : 
   Description : Client assembler provide client data    
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 19:43:43 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.ClientAssembler: 

    /*------------------------------------------------------------------------------
     Purpose: To get the registration type of the client
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER getRegistrationType( INPUT clientID AS INTEGER ):
        
        DEFINE VARIABLE result AS INTEGER NO-UNDO INIT 0.
        FIND FIRST client WHERE client.id EQ clientID NO-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN 
            RESULT = client.registration_type.

        RETURN result.

    END METHOD.

END CLASS.