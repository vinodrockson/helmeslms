 
/*------------------------------------------------------------------------
   File        : FineAssembler
   Purpose     : To provide functioanlity to loan.
   Syntax      : 
   Description : Fine assembler does operations for fines.    
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 20:40:28 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING models.RegistrationModel FROM PROPATH.
USING assemblers.ReasonAssembler FROM PROPATH.
USING assemblers.BookAssembler FROM PROPATH.
USING assemblers.ClientAssembler FROM PROPATH.
USING assemblers.RegistrationAssembler FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.FineAssembler: 

    /*------------------------------------------------------------------------------
     Purpose: To create invoice from the Loans
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER createInvoice( INPUT loanID AS INTEGER, INPUT bookID AS INTEGER, INPUT clientID AS INTEGER, 
        INPUT reasonID AS INTEGER , INPUT overdueDays AS INTEGER):
        
        DEFINE VARIABLE result        AS INTEGER   NO-UNDO INIT 0.
        DEFINE VARIABLE oClient       AS CLASS     ClientAssembler       NO-UNDO.
        DEFINE VARIABLE oRegistration AS CLASS     RegistrationAssembler NO-UNDO.
        DEFINE VARIABLE oBook         AS CLASS     BookAssembler         NO-UNDO.
        DEFINE VARIABLE oRegModel     AS CLASS     RegistrationModel     NO-UNDO.
        DEFINE VARIABLE oReason       AS CLASS     ReasonAssembler       NO-UNDO.
        DEFINE VARIABLE regType       AS INTEGER   NO-UNDO.
        DEFINE VARIABLE fineID        AS INTEGER   NO-UNDO.
        DEFINE VARIABLE cost          AS DECIMAL   NO-UNDO INIT 0.00.
        DEFINE VARIABLE reasonStr     AS CHARACTER NO-UNDO.
        
        oClient =  NEW ClientAssembler().
        oRegistration = NEW RegistrationAssembler().
        oBook = NEW BookAssembler().
        regType = oClient:getRegistrationType(clientID).
        oRegModel = oRegistration:getRegistrationById(regType). 
        fineID = NEXT-VALUE (FINE_ID_SEQ,lms).      
        
        IF INDEX(oReason:parseReason(reasonId),"DAMAGE") GT 0 THEN 
            cost = cost + (oBook:getBookById(bookID):price * oRegModel:damageCoefficient).
        
        IF INDEX(oReason:parseReason(reasonId),"LOST") GT 0 THEN 
        DO:
            cost = cost + (oBook:getBookById(bookID):price * oRegModel:lostCoefficient).
            overdueDays = 0.
        END.
        
        IF overdueDays GT 0 THEN 
            cost = cost + (overdueDays * oRegModel:overdueCoefficient).
         
        IF reasonID GT 0 THEN 
            reasonStr = reasonStr + STRING(reasonID).
            
        IF overdueDays GT 0 AND reasonID GT 0 THEN 
            reasonStr = reasonStr + "," + STRING(oReason:getReasonID("loan", "OVERDUE")). 
        ELSE 
            reasonStr = reasonStr + STRING(oReason:getReasonID("loan", "OVERDUE")).   
            
        CREATE fine. 
        ASSIGN
            fine.id              = fineID
            fine.loan_id         = loanID
            fine.registered_date = NOW
            fine.due_date        = NOW + oRegModel:fineInterval
            fine.cost            = cost
            fine.client_id       = clientID
            fine.reg_type_id     = regType
            fine.reason_id       = reasonStr 
            .
            
        RESULT = fineID.
        RETURN result.

    END METHOD.

END CLASS.