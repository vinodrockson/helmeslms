 
/*------------------------------------------------------------------------
   File        : LanguageAsembler
   Purpose     : To provide languages for the system
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Wed Jul 24 18:41:14 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.LanguageAssembler: 

    DEFINE TEMP-TABLE ttLanguages LIKE languages.
    
    /*------------------------------------------------------------------------------
     Purpose: Parse languages name
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER parseLanguage( INPUT iLanguageId AS INTEGER):
        DEFINE VARIABLE RESULT AS CHARACTER NO-UNDO INIT "".
        
        FIND FIRST languages WHERE languages.id EQ iLanguageId NO-LOCK NO-ERROR.
        IF AVAILABLE languages THEN
            ASSIGN 
                RESULT = languages.lang_text.
                
        RETURN RESULT.        
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Get all languages
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getAllLanguages(OUTPUT TABLE ttLanguages):
        EMPTY TEMP-TABLE ttLanguages.
        
        FOR EACH languages NO-LOCK:
            CREATE ttLanguages.
            BUFFER-COPY languages TO ttLanguages.      
        END.
                
        RETURN.        
    END METHOD.

END CLASS.