 
/*------------------------------------------------------------------------
   File        : LoanAssembler
   Purpose     : To perform loan functionalities
   Syntax      : 
   Description : Loan Assembler
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 11:50:31 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING models.ErrorModel FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.LoanAssembler: 
    
    /*------------------------------------------------------------------------------
     Purpose: To check the validity of the loan request
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC ErrorModel isValidLend( clientId AS INTEGER, bookIsbn AS CHARACTER, borrowFrequency AS INTEGER ):
        
        DEFINE VARIABLE result AS CLASS ErrorModel NO-UNDO.
            
        FIND FIRST loan WHERE loan.client_id EQ clientId AND loan.isbn EQ bookIsbn AND loan.has_returned EQ NO NO-LOCK NO-ERROR.        
        IF AVAILABLE loan THEN
            RESULT = NEW ErrorModel("You have similar copy of book already. You cannot take another copy.", YES).
        ELSE 
        DO: 
            FIND FIRST loan WHERE loan.client_id EQ clientId AND loan.isbn EQ bookIsbn AND loan.has_returned EQ YES NO-LOCK NO-ERROR.
            IF AVAILABLE loan THEN 
            DO:
                IF INTERVAL(loan.due_date, NOW, "days") GE borrowFrequency THEN
                    RESULT = NEW ErrorModel("", NO).
                ELSE
                    RESULT = NEW ErrorModel("You can borrow this book only after" + STRING(borrowFrequency - INTERVAL(loan.due_date, NOW, "days")) + " days. Upgrade your membership to increase the chance to take now.", YES).                     
            END.
            ELSE 
                RESULT = NEW ErrorModel("", NO).
        END.   
         
        RETURN result.

    END METHOD.

END CLASS.