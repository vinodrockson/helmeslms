 
/*------------------------------------------------------------------------
   File        : PublisherAssembler
   Purpose     : 
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Mon Jul 22 23:39:24 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.PublisherAssembler: 
    
    /*------------------------------------------------------------------------------
    Purpose: Process the publisher name(s) of book
    Notes:
   ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER processPublisher( INPUT iPublisher AS CHARACTER ):
        DEFINE VARIABLE i           AS INTEGER   NO-UNDO.
        DEFINE VARIABLE publisherID AS INTEGER   NO-UNDO.
        DEFINE VARIABLE RESULT      AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iPublisher):
            publisherID = THIS-OBJECT:createPublisher(TRIM(ENTRY(i,iPublisher))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + STRING(publisherID).    
        END.      
        
        RETURN RESULT.
        
    END METHOD.


    /*------------------------------------------------------------------------------
     Purpose: To create the publisher in DB
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER createPublisher(INPUT publisherName AS CHARACTER):
        DEFINE VARIABLE RESULT AS INTEGER NO-UNDO.
        DEFINE VARIABLE newID  AS INTEGER NO-UNDO.
        
        FIND FIRST publisher WHERE publisher.name = publisherName NO-LOCK NO-ERROR.
        
        IF AVAILABLE publisher THEN
            RESULT = publisher.id.
        ELSE 
        DO:
            newID = NEXT-VALUE (PUBLISHER_ID_SEQ,lms).
            
            CREATE publisher. 
            ASSIGN 
                publisher.id   = newID
                publisher.name = publisherName
                .
            RESULT = newID.
        END. 
        
        RETURN RESULT.
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Parse publisher Id(s) to Name
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER parsePublisherId( INPUT iPublisherId AS CHARACTER ):
        
        DEFINE VARIABLE i             AS INTEGER   NO-UNDO.
        DEFINE VARIABLE publisherName AS CHARACTER NO-UNDO.
        DEFINE VARIABLE RESULT        AS CHARACTER NO-UNDO.
        
        DO i=1 TO NUM-ENTRIES (iPublisherId):
            publisherName = THIS-OBJECT:getPublisherName(INTEGER(TRIM(ENTRY(i,iPublisherId)))).
            RESULT = RESULT + (IF i=1 THEN "" ELSE ",") + publisherName.    
        END.      
        
        RETURN RESULT.

    END METHOD.    

    /*------------------------------------------------------------------------------
     Purpose: To get publisher name by publisher Id
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER getPublisherName( INPUT publisherId AS INTEGER  ):
        
        DEFINE VARIABLE result AS CHARACTER NO-UNDO INITIAL "".
        
        FIND FIRST publisher WHERE publisher.id = publisherId NO-LOCK NO-ERROR.
        
        IF AVAILABLE publisher THEN
            RESULT = publisher.name.
      
        RETURN result.

    END METHOD.
END CLASS.