 
/*------------------------------------------------------------------------
   File        : ReasonAssembler
   Purpose     : 
   Syntax      : 
   Description : Reason assembler for suspend table
   Author(s)   : vinodrockson
   Created     : Tue Jul 23 21:40:45 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.ReasonAssembler: 
    
    DEFINE TEMP-TABLE ttReasons LIKE reason.
    
    /*------------------------------------------------------------------------------
     Purpose: Parse reason name for client
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC CHARACTER parseReason( INPUT iReasonId AS INTEGER):
        DEFINE VARIABLE RESULT AS CHARACTER NO-UNDO INIT "".
        
        FIND FIRST reason WHERE reason.id EQ iReasonId NO-LOCK NO-ERROR.
        IF AVAILABLE reason THEN
            ASSIGN 
                RESULT = reason.reason.
                
        RETURN RESULT.        
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Get all reasons for client
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC VOID getReasons( INPUT iType AS CHARACTER, OUTPUT TABLE ttReasons):
        EMPTY TEMP-TABLE ttReasons.
        
        FOR EACH reason WHERE reason.type EQ iType NO-LOCK:
            CREATE ttReasons.
            BUFFER-COPY reason TO ttReasons.      
        END.
                
        RETURN.        
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Get reasonID by reason name
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC INTEGER getReasonID( INPUT iType AS CHARACTER, INPUT reasonName AS CHARACTER):
        DEFINE VARIABLE RESULT AS INTEGER NO-UNDO INIT 0.
        
        FIND FIRST reason WHERE reason.type EQ iType AND INDEX(reason.reason, reasonName) GT 0 NO-LOCK NO-ERROR.
        IF AVAILABLE reason THEN
            ASSIGN 
                RESULT = reason.id.
                
        RETURN RESULT.        
    END METHOD.

END CLASS.