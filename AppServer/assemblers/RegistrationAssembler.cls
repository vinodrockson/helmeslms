 
/*------------------------------------------------------------------------
   File        : RegistrationAssembler
   Purpose     : To provide regsitration data
   Syntax      : 
   Description : Registration data processing
   Author(s)   : vinodrockson
   Created     : Wed Jul 24 22:15:26 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING assemblers.RegistrationAssembler FROM PROPATH.
USING models.RegistrationModel FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS assemblers.RegistrationAssembler: 
     
       
    /*------------------------------------------------------------------------------
     Purpose: To provide renewal interval for registration ID
     Notes:
    ------------------------------------------------------------------------------*/

    METHOD PUBLIC RegistrationModel getRegistrationById( INPUT resgitrationID AS INTEGER):
        DEFINE VARIABLE RESULT AS CLASS RegistrationModel NO-UNDO.

        FIND FIRST registration WHERE registration.id EQ resgitrationID NO-LOCK NO-ERROR.
        IF AVAILABLE registration THEN
            RESULT = NEW RegistrationModel(registration.registration_type, registration.renewal_times, registration.renewal_interval,
                registration.lost_coefficient, registration.damage_coefficient, registration.price, registration.fine_interval, 
                registration.overdue_gp, registration.fine_gp, registration.overdue_coefficient, registration.fine_coefficient, 
                registration.borrow_frequency,registration.borrow_limit).
                
        RETURN RESULT.

    END METHOD.

END CLASS.