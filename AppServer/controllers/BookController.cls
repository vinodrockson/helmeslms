@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
/*------------------------------------------------------------------------
   File        : BookController
   Purpose     : CRUD actions for  books
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Mon Jul 22 20:35:57 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING assemblers.CategoryAssembler FROM PROPATH.
USING assemblers.AuthorAssembler FROM PROPATH.
USING assemblers.PublisherAssembler FROM PROPATH.
USING assemblers.ReasonAssembler FROM PROPATH.
USING assemblers.LanguageAssembler FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS controllers.BookController: 
    DEFINE TEMP-TABLE getBook LIKE book.
    DEFINE TEMP-TABLE updateBook LIKE book.
    DEFINE TEMP-TABLE ttReasons LIKE reason.
    DEFINE TEMP-TABLE ttLanguages LIKE languages.
    
    DEFINE DATASET bookSet FOR updateBook.   
    
    /*------------------------------------------------------------------------------
     Purpose: To create a new book
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID registerBook(INPUT bookTitle AS CHARACTER, INPUT isbn AS CHARACTER, INPUT iCategory AS CHARACTER, INPUT languageID AS INTEGER,
        INPUT publishedDate AS DATE, INPUT price AS INTEGER, INPUT iAuthor AS CHARACTER, INPUT iPublisher AS CHARACTER,
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):   
                
        DEFINE VARIABLE categoryId  AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oCategory   AS CLASS     CategoryAssembler  NO-UNDO.
        DEFINE VARIABLE authorId    AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oAuthor     AS CLASS     AuthorAssembler    NO-UNDO.
        DEFINE VARIABLE publisherId AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oPublisher  AS CLASS     PublisherAssembler NO-UNDO.         
        
        oCategory = NEW CategoryAssembler().
        categoryId = oCategory:processCategory(iCategory).
                
        oAuthor = NEW AuthorAssembler().
        authorId = oAuthor:processAuthor(iAuthor).
        
        oPublisher = NEW PublisherAssembler().
        publisherId = oPublisher:processPublisher(iPublisher).   
       
                                      
        CREATE book. 
        ASSIGN 
            book.id              = NEXT-VALUE (BOOK_ID_SEQ,lms)
            book.book_title      = bookTitle
            book.isbn            = isbn            
            book.published_date  = publishedDate
            book.price           = price
            book.is_active       = YES
            book.registered_date = NOW 
            book.author_id       = authorId
            book.publisher_id    = publisherId
            book.category_id     = categoryId
            book.language_id     = languageID
            .
        
        returnMessage = "Book created successfully".
        returnFlag = TRUE.
        
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To get all books
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getAllBooks( OUTPUT TABLE getBook, OUTPUT TABLE ttReasons, OUTPUT TABLE ttLanguages, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL  ):     
               
        DEFINE VARIABLE categoryId     AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oCategory      AS CLASS     CategoryAssembler  NO-UNDO.
        DEFINE VARIABLE authorId       AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oAuthor        AS CLASS     AuthorAssembler    NO-UNDO.
        DEFINE VARIABLE publisherId    AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oPublisher     AS CLASS     PublisherAssembler NO-UNDO. 
        DEFINE VARIABLE reason         AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oReason        AS CLASS     ReasonAssembler    NO-UNDO.
        DEFINE VARIABLE reasonHandle   AS HANDLE    NO-UNDO. 
        DEFINE VARIABLE oLanguage      AS CLASS     LanguageAssembler  NO-UNDO.
        DEFINE VARIABLE languageHandle AS HANDLE    NO-UNDO. 
           
        EMPTY TEMP-TABLE getBook.
        reasonHandle = TEMP-TABLE ttReasons:HANDLE.
        languageHandle =  TEMP-TABLE ttLanguages:HANDLE.        
        
        FOR EACH book NO-LOCK:
            oCategory = NEW CategoryAssembler().
            categoryId = oCategory:parseCategoryId(book.category_id).
                
            oAuthor = NEW AuthorAssembler().
            authorId = oAuthor:parseAuthorId(book.author_id).
        
            oPublisher = NEW PublisherAssembler().
            publisherId = oPublisher:parsePublisherId(book.publisher_id).  
            
            CREATE getBook.
            BUFFER-COPY book EXCEPT book.author_id book.category_id book.publisher_id TO getBook.
            ASSIGN 
                getBook.author_id    = authorId
                getBook.category_id  = categoryId
                getBook.publisher_id = publisherId
                .           
        END.
        
        IF reasonHandle:HAS-RECORDS EQ FALSE THEN 
        DO:
            oReason = NEW ReasonAssembler().
            oReason:getReasons(INPUT "book", OUTPUT TABLE ttReasons). 
        END.  
        
        IF languageHandle:HAS-RECORDS EQ FALSE THEN 
        DO:
            oLanguage = NEW LanguageAssembler().
            oLanguage:getAllLanguages(OUTPUT TABLE ttLanguages). 
        END.  
        
        returnMessage = "Books retrived succcessfully".
        returnFlag = TRUE.
        
        IF VALID-OBJECT(reasonHandle) THEN
            DELETE OBJECT reasonHandle. 
            
        IF VALID-OBJECT(languageHandle) THEN
            DELETE OBJECT languageHandle. 
            
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To get one book by book ID
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getOneBook(INPUT bookID AS INTEGER, OUTPUT TABLE getBook, OUTPUT TABLE ttReasons, OUTPUT TABLE ttLanguages, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
        
        DEFINE VARIABLE categoryId     AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oCategory      AS CLASS     CategoryAssembler  NO-UNDO.
        DEFINE VARIABLE authorId       AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oAuthor        AS CLASS     AuthorAssembler    NO-UNDO.
        DEFINE VARIABLE publisherId    AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oPublisher     AS CLASS     PublisherAssembler NO-UNDO. 
        DEFINE VARIABLE reason         AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oReason        AS CLASS     ReasonAssembler    NO-UNDO. 
        DEFINE VARIABLE reasonHandle   AS HANDLE    NO-UNDO.  
        DEFINE VARIABLE oLanguage      AS CLASS     LanguageAssembler  NO-UNDO.
        DEFINE VARIABLE languageHandle AS HANDLE    NO-UNDO.   
           
        EMPTY TEMP-TABLE getBook.
        reasonHandle = TEMP-TABLE ttReasons:HANDLE.
        languageHandle =  TEMP-TABLE ttLanguages:HANDLE.  
        
        FIND FIRST book WHERE book.id EQ bookID NO-LOCK NO-ERROR.
        
        IF AVAILABLE book THEN 
        DO:   
            oCategory = NEW CategoryAssembler().
            categoryId = oCategory:parseCategoryId(book.category_id).
                
            oAuthor = NEW AuthorAssembler().
            authorId = oAuthor:parseAuthorId(book.author_id).
        
            oPublisher = NEW PublisherAssembler().
            publisherId = oPublisher:parsePublisherId(book.publisher_id).  
                     
            CREATE getBook.
            BUFFER-COPY  book EXCEPT book.author_id book.category_id book.publisher_id TO getBook.       
            ASSIGN 
                getBook.author_id    = authorId
                getBook.category_id  = categoryId
                getBook.publisher_id = publisherId
                .          
                
            IF reasonHandle:HAS-RECORDS EQ FALSE THEN 
            DO:
                oReason = NEW ReasonAssembler().
                oReason:getReasons(INPUT "book", OUTPUT TABLE ttReasons). 
            END. 
            
            IF languageHandle:HAS-RECORDS EQ FALSE THEN 
            DO:
                oLanguage = NEW LanguageAssembler().
                oLanguage:getAllLanguages(OUTPUT TABLE ttLanguages). 
            END. 
            
            returnMessage = "Book retrieved successfully".
            returnFlag = TRUE.
        END. 
        ELSE 
        DO:
            returnMessage = "Book not found".
            returnFlag = FALSE.
        END.  
        
        IF VALID-OBJECT(reasonHandle) THEN
            DELETE OBJECT reasonHandle.  
        
        IF VALID-OBJECT(languageHandle) THEN
            DELETE OBJECT languageHandle. 
                  
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
    Purpose: To update a book record
    Notes:
   ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID updateBook( INPUT bookId AS INTEGER, INPUT DATASET bookSet, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
        
        DEFINE VARIABLE categoryId  AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oCategory   AS CLASS     CategoryAssembler  NO-UNDO.
        DEFINE VARIABLE authorId    AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oAuthor     AS CLASS     AuthorAssembler    NO-UNDO.
        DEFINE VARIABLE publisherId AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oPublisher  AS CLASS     PublisherAssembler NO-UNDO.         
                
        FIND FIRST updateBook.
        
        FIND FIRST book WHERE book.id EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        
        oCategory = NEW CategoryAssembler().
        categoryId = oCategory:processCategory(updateBook.category_id).
                
        oAuthor = NEW AuthorAssembler().
        authorId = oAuthor:processAuthor(updateBook.author_id).
        
        oPublisher = NEW PublisherAssembler().
        publisherId = oPublisher:processPublisher(updateBook.publisher_id). 
        
        ASSIGN             
            updateBook.id           = bookId
            updateBook.category_id  = categoryId
            updateBook.author_id    = authorId
            updateBook.publisher_id = publisherId
            updateBook.updated_date = NOW
            .
        
        IF AVAILABLE book THEN 
        DO:
            BUFFER-COPY updateBook TO book.
            returnMessage = "Book updated".
            returnFlag = TRUE.
        END.
        ELSE 
        DO:
            returnMessage = "Book not found".
            returnFlag = FALSE.              
        END.   
            
        RETURN.

    END METHOD.    
    
    /*------------------------------------------------------------------------------
     Purpose: Delete book
     Notes: This method doesn't delete the record. Instead make it inactive so that 
     a book's record shall be reopened later
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID deleteBook( INPUT bookId AS INTEGER, INPUT reasonID AS INTEGER, INPUT clientID AS INTEGER, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):
            
        FIND FIRST book WHERE book.id EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE book THEN 
        DO:    
            IF book.is_active EQ NO THEN 
            DO:
                returnMessage = "Book is already inactive".
                returnFlag = FALSE.
            END.
            ELSE 
            DO:                
                ASSIGN 
                    book.is_active          = NO
                    book.inactive_reason_id = reasonID
                    book.inactive_accused   = clientID
                    book.updated_date       = NOW
                    returnMessage           = "Book Deleted"
                    returnFlag              = TRUE
                    .    
            END.
        END. 
        ELSE 
        DO: 
            returnMessage = "Book not found".
            returnFlag = FALSE.   
        END.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: To reopen the closed book account
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID reopenBook( INPUT bookId AS INTEGER, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):
        FIND FIRST book WHERE book.id EQ bookId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE book THEN 
        DO:  
            IF book.is_active EQ YES THEN 
            DO:
                returnMessage = "Book is already active".
                returnFlag = FALSE.
            END.
            ELSE 
            DO:   
                ASSIGN 
                    book.is_active          = YES 
                    book.inactive_reason_id = ?
                    book.inactive_accused   = ?
                    book.updated_date       = NOW
                    returnMessage           = "Book account reopened"
                    returnFlag              = TRUE
                    . 
            END.   
        END. 
        ELSE 
        DO: 
            returnMessage = "Book not found".
            returnFlag = FALSE.   
        END.
        RETURN.
    END METHOD.
END CLASS.