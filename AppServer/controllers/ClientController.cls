@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
/*------------------------------------------------------------------------
   File        : ClientController
   Purpose     : To provide services for clients.
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Mon Jul 22 02:01:55 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/


BLOCK-LEVEL ON ERROR UNDO, THROW.

USING assemblers.ReasonAssembler FROM PROPATH.

CLASS controllers.ClientController: 
    
    DEFINE TEMP-TABLE clients LIKE client.
    DEFINE TEMP-TABLE reasons LIKE reason.
    
    DEFINE DATASET clientSet FOR clients.
    

    /*------------------------------------------------------------------------------
     Purpose: To register a client
     Notes:
    ------------------------------------------------------------------------------*/
    
    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID registerClient(INPUT DATASET clientSet, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
         
        FIND FIRST clients.
        MESSAGE clients.first_name.        
        ASSIGN 
            clients.id               = NEXT-VALUE (CLIENT_ID_SEQ,lms)
            clients.registered_date  = NOW
            clients.is_active        = YES
            clients.access_status_id = 0
            .
            
        CREATE client.
        BUFFER-COPY clients TO client.
        
        returnMessage = "Client created successfully".
        returnFlag = TRUE.
        
        RETURN.
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: Get all clients
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getAllClients( OUTPUT TABLE clients, OUTPUT TABLE reasons, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ):
        
        DEFINE VARIABLE reason       AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oReason      AS CLASS     ReasonAssembler NO-UNDO. 
        DEFINE VARIABLE reasonHandle AS HANDLE    NO-UNDO.
        
        EMPTY TEMP-TABLE clients.
        reasonHandle = TEMP-TABLE reasons:HANDLE.
        
        FOR EACH client NO-LOCK:
            CREATE clients.
            BUFFER-COPY client TO clients.           
        END.
        
        IF reasonHandle:HAS-RECORDS EQ FALSE THEN 
        DO:
            oReason = NEW ReasonAssembler().
            oReason:getReasons(INPUT "client", OUTPUT TABLE reasons). 
        END.
        
        returnMessage = "Clients retrived succcessfully".
        returnFlag = TRUE.
        MESSAGE returnMessage.
        IF VALID-OBJECT(reasonHandle) THEN
            DELETE OBJECT reasonHandle.    
            
        RETURN.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: Get one specific client
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getSingleClient(INPUT clientID AS INTEGER, OUTPUT TABLE clients, OUTPUT TABLE reasons, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
                                           
        DEFINE VARIABLE reason       AS CHARACTER NO-UNDO.
        DEFINE VARIABLE oReason      AS CLASS     ReasonAssembler NO-UNDO. 
        DEFINE VARIABLE reasonHandle AS HANDLE    NO-UNDO.
        
        EMPTY TEMP-TABLE clients.
        reasonHandle = TEMP-TABLE reasons:HANDLE.
        
        FIND FIRST client WHERE client.id EQ clientID NO-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN 
        DO:            
            CREATE clients.
            BUFFER-COPY client TO clients.        
            
            IF reasonHandle:HAS-RECORDS EQ FALSE THEN 
            DO:
                oReason = NEW ReasonAssembler().
                oReason:getReasons(INPUT "client", OUTPUT TABLE reasons).  
            END.    
                
            returnMessage = "Client retrieved successfully".
            returnFlag = TRUE.
  
        END. 
        ELSE 
        DO:
            returnMessage = "Client not found".
            returnFlag = FALSE.
        END.
        
        IF VALID-OBJECT(reasonHandle) THEN
            DELETE OBJECT reasonHandle.    
           
        RETURN.

    END METHOD.    

    /*------------------------------------------------------------------------------
     Purpose: To update a client record
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID updateClient( INPUT clientId AS INTEGER, INPUT DATASET clientSet,
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):        
        
        FIND FIRST clients.
        
        FIND FIRST client WHERE client.id EQ clientId EXCLUSIVE-LOCK NO-ERROR.
        
        ASSIGN 
            clients.id           = clientId
            clients.updated_date = NOW
            .
        
        IF AVAILABLE client THEN 
        DO:
            BUFFER-COPY clients TO client.
            returnMessage = "Client updated".
            returnFlag = TRUE.
        END.
        ELSE 
        DO:
            returnMessage = "Client not found".
            returnFlag = FALSE.             
        END.   
            
        RETURN.

    END METHOD.    
    
    /*------------------------------------------------------------------------------
     Purpose: Delete client
     Notes: This method doesn't delete the record. Instead make it inactive so that 
     a client's record shall be reopened later
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID deleteClient( INPUT clientId AS INTEGER, INPUT reasonId AS INTEGER, INPUT accessId AS INTEGER, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):

        FIND FIRST client WHERE client.id EQ clientId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN 
        DO:  
            IF client.is_active EQ NO THEN 
            DO:
                returnMessage = "Client is already inactive".
                returnFlag = FALSE.
            END.  
            ELSE 
            DO:                    
                ASSIGN 
                    client.is_active         = NO
                    client.suspend_reason_id = reasonId
                    client.access_status_id  = accessId
                    client.updated_date      = NOW
                    returnMessage            = "Client Deleted"
                    returnFlag               = TRUE
                    .    
            END.
        END. 
        ELSE 
        DO: 
            returnMessage = "Client not found".
            returnFlag = FALSE.   
        END.
        RETURN.
    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To reopen the closed client account
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID reopenClient( INPUT clientId AS INTEGER, INPUT accessId AS INTEGER,
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):

        FIND FIRST client WHERE client.id EQ clientId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE client THEN 
        DO:
            IF client.is_active EQ YES THEN 
            DO:
                returnMessage = "Client is already active".
                returnFlag = FALSE.
            END.  
            ELSE 
            DO:     
                ASSIGN 
                    client.is_active         = YES 
                    client.access_status_id  = accessId
                    client.suspend_reason_id = ?
                    client.updated_date      = NOW
                    returnMessage            = "Client account reopened"
                    returnFlag               = TRUE
                    .   
            END. 
        END. 
        ELSE 
        DO: 
            returnMessage = "Client not found".
            returnFlag = FALSE.   
        END.
        RETURN.
    END METHOD.
    
END CLASS.