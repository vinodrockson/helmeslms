@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
/*------------------------------------------------------------------------
   File        : FineController
   Purpose     : 
   Syntax      : 
   Description : Fine Controller to handle invoice operations
   Author(s)   : vinodrockson
   Created     : Fri Jul 26 00:24:44 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS controllers.FineController: 

    DEFINE TEMP-TABLE ttFine LIKE fine.
    DEFINE TEMP-TABLE updateInvoice LIKE fine.
    
    DEFINE DATASET invoiceSet FOR updateInvoice.
     
    /*------------------------------------------------------------------------------
     Purpose: To get all invoices
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getAllInvoices( OUTPUT TABLE ttFine, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ) :
        
        EMPTY TEMP-TABLE ttFine.
    
        FOR EACH fine NO-LOCK:
            CREATE ttFine.
            BUFFER-COPY fine TO ttFine.           
        END.
        
        returnMessage = "Invoices retrived succcessfully".
        returnFlag = TRUE.
        
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To get invoices by pay status of invoice
     Notes: 
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getInvoiceByStatus( INPUT clientID AS INTEGER, INPUT isPaid AS LOGICAL, OUTPUT TABLE ttFine, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ) :
        
        EMPTY TEMP-TABLE ttFine.
    
        FOR EACH fine WHERE fine.client_id EQ clientID AND fine.is_paid EQ isPaid NO-LOCK:
            CREATE ttFine.
            BUFFER-COPY fine TO ttFine.           
        END.
        
        returnMessage = "Invoices retrived succcessfully".
        returnFlag = TRUE.
         
        RETURN.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: To get details of one invoice
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getOneInvoice( INPUT clientID AS INTEGER, INPUT fineID AS INTEGER, OUTPUT TABLE ttFine, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ):

        EMPTY TEMP-TABLE ttFine.
    
        FIND FIRST fine WHERE fine.id EQ fineID AND fine.client_id EQ clientID NO-LOCK NO-ERROR.
        IF AVAILABLE fine THEN
        DO:
            CREATE ttFine.
            BUFFER-COPY fine TO ttFine.   
            returnMessage = "Invoice retrived succcessfully".
            returnFlag = TRUE.        
        END.
        ELSE 
        DO:
            returnMessage = "Invoice not found".
            returnFlag = FALSE.
        END.
                 
        RETURN.
    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: To change the pay status of invoice
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID setInvoicePaid( INPUT clientID AS INTEGER, INPUT fineID AS INTEGER, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ):

        FIND FIRST fine WHERE fine.id EQ fineID AND fine.client_id EQ clientID EXCLUSIVE-LOCK NO-ERROR.
        IF AVAILABLE fine THEN
        DO:
            ASSIGN 
                fine.is_paid     = YES
                fine.update_date = NOW 
                returnMessage    = "Invoice status changed succcessfully."
                returnFlag       = TRUE
                .        
        END.
        ELSE 
        DO:
            returnMessage = "Invoice not found".
            returnFlag = FALSE.
        END.
                 
        RETURN.
    END METHOD.
    
    /*------------------------------------------------------------------------------
    Purpose: To update a invoice record
    Notes:
   ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID updateInvoice( INPUT invoiceId AS INTEGER, INPUT DATASET invoiceSet,
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):        
        
        FIND FIRST updateInvoice.
        
        FIND FIRST fine WHERE fine.id EQ invoiceId EXCLUSIVE-LOCK NO-ERROR.
        
        ASSIGN 
            updateInvoice.id          = invoiceId
            updateInvoice.update_date = NOW
            .
        
        IF AVAILABLE fine THEN 
        DO:
            BUFFER-COPY updateInvoice TO fine.
            returnMessage = "Invoice updated".
            returnFlag = TRUE.
        END.
        ELSE 
        DO:
            returnMessage = "Invoice not found".
            returnFlag = FALSE.             
        END.   
            
        RETURN.

    END METHOD.   
    
    /*------------------------------------------------------------------------------
     Purpose: Delete invoice
     Notes: 
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID deleteClient( INPUT fineId AS INTEGER, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):

        FIND FIRST fine WHERE fine.id EQ fineId EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE fine THEN 
        DO:   
            DELETE fine.
            returnMessage            = "Invoice Deleted".
            returnFlag               = TRUE.
        END. 
        ELSE 
        DO: 
            returnMessage = "Invoice not found".
            returnFlag = FALSE.   
        END.
        RETURN.
    END METHOD. 
END CLASS.