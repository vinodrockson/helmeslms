@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
/*------------------------------------------------------------------------
   File        : LoanController
   Purpose     : To provide REST APIs for book lending
   Syntax      : 
   Description : Operations for book lending
   Author(s)   : vinodrockson
   Created     : Wed Jul 24 21:31:28 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.
USING assemblers.RegistrationAssembler FROM PROPATH.
USING assemblers.BookAssembler FROM PROPATH.
USING assemblers.ReasonAssembler FROM PROPATH.
USING assemblers.AuthorAssembler FROM PROPATH.
USING models.RegistrationModel FROM PROPATH.
USING models.BookModel FROM PROPATH.
USING models.ErrorModel FROM PROPATH.
USING assemblers.LoanAssembler FROM PROPATH.
USING assemblers.ClientAssembler FROM PROPATH.
USING assemblers.FineAssembler FROM PROPATH.
USING controllers.BookController FROM PROPATH.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS controllers.LoanController: 
    DEFINE TEMP-TABLE giveLoan NO-UNDO
        FIELD id         AS INTEGER
        FIELD bookTitle  AS CHARACTER
        FIELD bookAuthor AS CHARACTER
        FIELD bookIsbn   AS CHARACTER
        FIELD bookStatus AS CHARACTER
        INDEX id id ASC.
    
    DEFINE TEMP-TABLE getLoans LIKE loan.
                
    /*------------------------------------------------------------------------------
     Purpose: To borrow book(s) from the library
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID borrowBook( INPUT clientId AS INTEGER, INPUT bookId AS CHARACTER, OUTPUT TABLE giveLoan, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
        
        DEFINE VARIABLE i             AS INTEGER   NO-UNDO.
        DEFINE VARIABLE j             AS INTEGER   NO-UNDO.
        DEFINE VARIABLE activeBooks   AS INTEGER   NO-UNDO INIT 0.
        DEFINE VARIABLE totalBooks    AS INTEGER   NO-UNDO.
        DEFINE VARIABLE tempBookId    AS INTEGER   NO-UNDO.
        DEFINE VARIABLE oRegistration AS CLASS     RegistrationAssembler NO-UNDO.
        DEFINE VARIABLE oRegModel     AS CLASS     RegistrationModel     NO-UNDO.
        DEFINE VARIABLE oErrModel     AS CLASS     ErrorModel            NO-UNDO.
        DEFINE VARIABLE oBook         AS CLASS     BookAssembler         NO-UNDO.
        DEFINE VARIABLE oBookModel    AS CLASS     BookModel             NO-UNDO.
        DEFINE VARIABLE oAuthor       AS CLASS     AuthorAssembler       NO-UNDO.
        DEFINE VARIABLE oReason       AS CLASS     ReasonAssembler       NO-UNDO.
        DEFINE VARIABLE oLoan         AS CLASS     LoanAssembler         NO-UNDO.
        DEFINE VARIABLE loanDate      AS DATE      NO-UNDO.
        DEFINE VARIABLE dueDate       AS DATE      NO-UNDO.       
        DEFINE VARIABLE bookStatus    AS CHARACTER NO-UNDO.   
        
        EMPTY TEMP-TABLE giveLoan.
              
        FIND FIRST client WHERE client.id EQ clientId NO-LOCK NO-ERROR.
        IF AVAILABLE client THEN
        DO:   
            IF client.is_active THEN 
            DO:   
                oRegistration = NEW RegistrationAssembler().
                oRegModel = oRegistration:getRegistrationById(INPUT client.registration_type).  
                FOR EACH loan WHERE loan.client_id EQ client.id AND loan.has_returned EQ NO NO-LOCK:
                    activeBooks = activeBooks + 1.
                END.
                                
                IF activeBooks LT oRegModel:borrowLimit THEN 
                DO:                               
                    loanDate = NOW.
                    dueDate = ADD-INTERVAL (NOW, oRegModel:renewalInterval, "days").
                    j=0.   
                    totalBooks = NUM-ENTRIES (bookId).  
                    DO i=1 TO totalBooks: 
                        tempBookId = INTEGER(TRIM(ENTRY(i,bookId))).
                        oBook = NEW BookAssembler().
                        oBookModel = oBook:getBookById(INPUT tempBookId).
                        oLoan = NEW LoanAssembler().
                        oErrModel = oLoan:isValidLend( client.id, oBookModel:isbn, oRegModel:borrowFrequency).
                        IF oBookModel:isActive AND NOT(oErrModel:hasError) THEN 
                        DO:   
                            CREATE loan.
                            ASSIGN 
                                loan.id                = NEXT-VALUE (LOAN_ID_SEQ,lms)
                                loan.client_id         = client.id  
                                loan.book_id           = tempBookId
                                loan.loan_date         = loanDate
                                loan.due_date          = dueDate
                                loan.renewal_remaining = oRegModel:renewalTimes   
                                loan.has_returned      = NO
                                loan.isbn              = oBookModel:isbn        
                                .
                        END.
                        ELSE 
                        DO:
                            oAuthor = NEW AuthorAssembler().
                            oReason =  NEW ReasonAssembler().
                            j = j + 1.
                            IF oErrModel:hasError THEN 
                                bookStatus = oErrModel:errorMessage.
                            ELSE 
                                bookStatus = oReason:parseReason(oBookModel:reasonId).
                                
                            CREATE giveLoan.
                            ASSIGN 
                                giveLoan.id         = j
                                giveLoan.bookTitle  = oBookModel:bookTitle
                                giveLoan.bookAuthor = oAuthor:parseAuthorId(oBookModel:authorId)
                                giveLoan.bookIsbn   = oBookModel:isbn
                                giveLoan.bookStatus = bookStatus
                                .                                                   
                        END.                 
                    END.
                    
                    IF j EQ 0 AND totalBooks GE 1 THEN
                    DO:
                        IF totalBooks EQ 1 THEN 
                            returnMessage = "The book has been registered successfully. Check details of borrow here".
                        ELSE 
                            returnMessage = "The books are registered successfully. Check details of borrow here".
                        returnFlag = TRUE.
                    END.     
                    ELSE IF j EQ totalBooks THEN 
                    DO:
                        IF totalBooks EQ 1 THEN 
                            returnMessage = "This book cannot be borrowed for the following reason. Please find another copy of the book. Search for availability here".
                        ELSE 
                            returnMessage = "The books you requested cannot be borrowed. Please find another copy of the book. Search for availability here".
                        returnFlag = FALSE.
                    END.
                    ELSE IF j GT 0 AND j LT totalBooks THEN 
                    DO:
                        returnMessage = "The books has been registered successfully. The books mentioned below cannot be borrowed. Please find another copy of these books. Search for availability here".
                        returnFlag = TRUE.
                    END.
                END.
                ELSE 
                DO:
                    returnMessage = "Sorry! You have reached the limit to borrow books.".
                    returnFlag = FALSE.
                END.             
            END.
            ELSE 
            DO:
                returnMessage = "Your account is inactive. You cannot borrow any book. Please contact administator at admin@helmlib.ee.".
                returnFlag = FALSE.
            END.
        END.
        ELSE 
        DO:
            returnMessage = "Your account not found in our system. Please contact administator at admin@helmlib.ee.".
            returnFlag = FALSE.        
        END.       
        RETURN.

    END METHOD.

    /*------------------------------------------------------------------------------
     Purpose: To get list of loans of all clients
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getAllLoans( OUTPUT TABLE getLoans, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ) :
        
        EMPTY TEMP-TABLE getLoans.
    
        FOR EACH loan NO-LOCK:
            CREATE getLoans.
            BUFFER-COPY loan TO getLoans.           
        END.
        
        returnMessage = "Loans retrived succcessfully".
        returnFlag = TRUE.
         
        RETURN.

    END METHOD.
            
    /*------------------------------------------------------------------------------
     Purpose:  Get Loan records by return status. 
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getLoanByStatus( INPUT clientID AS INTEGER, INPUT hasReturned AS LOGICAL, OUTPUT TABLE getLoans, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ):

        EMPTY TEMP-TABLE getLoans.
    
        FOR EACH loan WHERE loan.client_id EQ clientID AND loan.has_returned EQ hasReturned NO-LOCK:
            CREATE getLoans.
            BUFFER-COPY loan TO getLoans.           
        END.
        
        returnMessage = "Loans retrived succcessfully".
        returnFlag = TRUE.
         
        RETURN.

    END METHOD.
    
    /*------------------------------------------------------------------------------
     Purpose: To get details of  one loan
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID getOneLoan( INPUT clientID AS INTEGER, INPUT loanID AS INTEGER, OUTPUT TABLE getLoans, OUTPUT returnMessage AS CHARACTER,
        OUTPUT returnFlag AS LOGICAL ):

        EMPTY TEMP-TABLE getLoans.
    
        FIND FIRST loan WHERE loan.id EQ loanID AND loan.client_id EQ clientID NO-LOCK NO-ERROR.
        IF AVAILABLE loan THEN
        DO:
            CREATE getLoans.
            BUFFER-COPY loan TO getLoans.   
            returnMessage = "Loan retrived succcessfully".
            returnFlag = TRUE.        
        END.
        ELSE 
        DO:
            returnMessage = "Loan not found".
            returnFlag = FALSE.
        END.
                 
        RETURN.

    END METHOD.
    
    /*------------------------------------------------------------------------------
    Purpose: To renew a lon by client and loan IDs
    Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID renewLoan( INPUT clientID AS INTEGER, INPUT loanID AS INTEGER, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):
        
        DEFINE VARIABLE renewalRemaining AS INTEGER NO-UNDO INIT 0.
        DEFINE VARIABLE renewalInterval  AS INTEGER NO-UNDO INIT 0.
        DEFINE VARIABLE oClient          AS CLASS   ClientAssembler       NO-UNDO.
        DEFINE VARIABLE oRegistration    AS CLASS   RegistrationAssembler NO-UNDO.
        
        FIND FIRST loan WHERE loan.id EQ loanID AND loan.client_id EQ clientID EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE loan THEN 
        DO:
            oClient =  NEW ClientAssembler().
            oRegistration = NEW RegistrationAssembler().
            renewalInterval = oRegistration:getRegistrationById(oClient:getRegistrationType(clientID)):renewalInterval.
            IF loan.renewal_remaining GT 0 THEN 
                renewalRemaining = loan.renewal_remaining - 1. 
                
            ASSIGN 
                loan.due_date          = NOW + renewalInterval
                loan.renewal_remaining = renewalRemaining
                loan.updated_date      = NOW             
                returnMessage          = "This borrowed book has been renewed successfully. Check renew details here"
                returnFlag             = TRUE
                .
        END.
        ELSE 
        DO:
            returnMessage = "Cannot renew this book. Please try again later. ".
            returnFlag = FALSE.             
        END.   
           
        RETURN.

    END METHOD. 
    
    /*------------------------------------------------------------------------------
    Purpose: To return a loan or to report if book is lost
    Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID returnLoan( INPUT clientID AS INTEGER, INPUT loanID AS INTEGER, INPUT reasonID AS INTEGER, 
        OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL ):

        DEFINE VARIABLE renewalInterval AS INTEGER   NO-UNDO INIT 0.
        DEFINE VARIABLE hasFine         AS LOGICAL   NO-UNDO INIT NO.
        DEFINE VARIABLE invoiceID       AS INTEGER   NO-UNDO.
        DEFINE VARIABLE oFine           AS CLASS     FineAssembler  NO-UNDO.
        DEFINE VARIABLE oBook           AS CLASS     BookController NO-UNDO.
        DEFINE VARIABLE bookMessage     AS CHARACTER NO-UNDO.
        DEFINE VARIABLE bookFlag        AS LOGICAL   NO-UNDO.
        
        FIND FIRST loan WHERE loan.id EQ loanID AND loan.client_id EQ clientID EXCLUSIVE-LOCK NO-ERROR.
        
        IF AVAILABLE loan THEN 
        DO:           
            IF reasonID GT 0 OR loan.overdue_days GT 0 THEN 
            DO:
                oFine = NEW FineAssembler().
                invoiceID = oFine:createInvoice(loan.id, loan.book_id, loan.client_id, reasonID, loan.overdue_days).
                IF invoiceID GT 0 THEN 
                    hasFine = YES.            
            END.  
            
            IF reasonID GT 0 THEN 
            DO:
                oBook = NEW BookController().
                oBook:deleteBook(loan.book_id, reasonID, loan.client_id, bookMessage, bookFlag).           
            END.  
                  
            ASSIGN 
                loan.due_date     = NOW
                loan.updated_date = NOW             
                loan.has_returned = YES 
                loan.has_fine     = hasFine
                loan.fine_inv_id  = invoiceID        
                loan.reason_id    = reasonID         
                returnMessage     = "This borrowed book has been returned successfully. Check renew details here"
                returnFlag        = TRUE
                .
        END.
        ELSE 
        DO:
            returnMessage = "Cannot return this book now. Please try again later. ".
            returnFlag = FALSE.             
        END.   
           
        RETURN.
    END METHOD. 
    
END CLASS.