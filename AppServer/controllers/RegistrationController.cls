@openapi.openedge.export FILE(type="REST", executionMode="singleton", useReturnValue="false", writeDataSetBeforeImage="false").
 
/*------------------------------------------------------------------------
   File        : RegistrationController
   Purpose     : 
   Syntax      : 
   Description : Registration Controller to handle registration types
   Author(s)   : vinodrockson
   Created     : Fri Jul 26 01:47:23 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS controllers.RegistrationController: 

    /*------------------------------------------------------------------------------
     Purpose: To create registration
     Notes:
    ------------------------------------------------------------------------------*/

    @openapi.openedge.export(type="REST", useReturnValue="false", writeDataSetBeforeImage="false").
    METHOD PUBLIC VOID createRegistration( INPUT registrationType AS CHARACTER, INPUT renewalTimes AS INTEGER, INPUT renewalInterval AS INTEGER, 
        INPUT lostCoefficient AS DECIMAL, INPUT damageCoefficient AS DECIMAL, INPUT price AS DECIMAL, INPUT fineInterval AS INTEGER,
        INPUT overdueGp AS INTEGER, INPUT fineGp AS INTEGER, INPUT overdueCoefficient AS DECIMAL, INPUT fineCoefficient AS DECIMAL,
        INPUT borrowFrequency AS INTEGER, INPUT borrowLimit AS INTEGER, OUTPUT returnMessage AS CHARACTER, OUTPUT returnFlag AS LOGICAL):
        
        CREATE registration.
        ASSIGN 
            registration.id =  NEXT-VALUE (REGISTRATION_ID_SEQ,lms)
            registration.registration_type =  registrationType
            registration.renewal_times = renewalTimes
            registration.renewal_interval = renewalInterval
            registration.lost_coefficient = lostCoefficient
            registration.damage_coefficient = damageCoefficient
            registration.price =  price
            registration.fine_interval =  fineInterval
            registration.overdue_gp = overdueGp
            registration.fine_gp = fineGp
            registration.overdue_coefficient = overdueCoefficient
            registration.fine_coefficient = fineCoefficient
            registration.borrow_frequency = borrowFrequency
            registration.borrow_limit = borrowLimit
            registration.registered_date =  NOW 
            returnMessage = "Regsitration created sucessfully"
            returnFlag = TRUE
            .      
        
        RETURN.

    END METHOD.

END CLASS.