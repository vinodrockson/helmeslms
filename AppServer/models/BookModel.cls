 
/*------------------------------------------------------------------------
   File        : BookModel
   Purpose     : To provide data for Book Object
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 04:20:25 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS models.BookModel: 
    DEFINE PROPERTY bookTitle AS CHARACTER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY isbn AS CHARACTER INITIAL ? NO-UNDO GET.
        PRIVATE SET.    
    DEFINE PROPERTY publishedDate AS DATETIME INITIAL ? NO-UNDO GET.
        PRIVATE SET.       
    DEFINE PROPERTY isActive AS LOGICAL INITIAL ? NO-UNDO GET.
        PRIVATE SET.
    DEFINE PROPERTY registeredDate AS DATETIME INITIAL ? NO-UNDO GET.
        PRIVATE SET.  
    DEFINE PROPERTY updatedDate AS DATETIME INITIAL ? NO-UNDO GET.
        PRIVATE SET.            
    DEFINE PROPERTY authorId AS CHARACTER INITIAL ? NO-UNDO GET.
        PRIVATE SET.    
    DEFINE PROPERTY publisherId AS CHARACTER INITIAL ? NO-UNDO GET.
        PRIVATE SET.    
    DEFINE PROPERTY categoryId AS CHARACTER INITIAL ? NO-UNDO GET.
        PRIVATE SET.    
    DEFINE PROPERTY reasonId AS INTEGER INITIAL ? NO-UNDO GET.
        PRIVATE SET. 
    DEFINE PROPERTY accusedId AS INTEGER INITIAL ? NO-UNDO GET.
        PRIVATE SET.      
    DEFINE PROPERTY languageId AS INTEGER INITIAL ? NO-UNDO GET.
        PRIVATE SET.  
    DEFINE PROPERTY price AS DECIMAL INITIAL ? NO-UNDO GET.
        PRIVATE SET.  

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC BookModel (
        bookTitle AS CHARACTER,
        isbn AS CHARACTER,    
        publishedDate AS DATETIME,       
        isActive AS LOGICAL,
        registeredDate AS DATETIME,  
        updatedDate AS DATETIME,            
        authorId AS CHARACTER,    
        publisherId AS CHARACTER,    
        categoryId AS CHARACTER,    
        reasonId AS INTEGER, 
        accusedId AS INTEGER,      
        languageId AS INTEGER,  
        price AS DECIMAL)
        :
        THIS-OBJECT:bookTitle = bookTitle.
        THIS-OBJECT:isbn = isbn.
        THIS-OBJECT:publishedDate = publishedDate.    
        THIS-OBJECT:isActive = isActive.
        THIS-OBJECT:registeredDate = registeredDate.
        THIS-OBJECT:updatedDate = updatedDate.         
        THIS-OBJECT:authorId = authorId.  
        THIS-OBJECT:publisherId = publisherId.  
        THIS-OBJECT:categoryId = categoryId.  
        THIS-OBJECT:reasonId = reasonId.        
        THIS-OBJECT:accusedId = accusedId.  
        THIS-OBJECT:languageId = languageId.  
        THIS-OBJECT:price = price.        
    END CONSTRUCTOR.
        
        
END CLASS.