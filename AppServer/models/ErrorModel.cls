 
/*------------------------------------------------------------------------
   File        : ErrorModel
   Purpose     : To handle  error messages 
   Syntax      : 
   Description : Common model for error handling
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 11:44:16 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS models.ErrorModel: 

    DEFINE PROPERTY errorMessage AS CHARACTER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY hasError AS LOGICAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC ErrorModel ( errorMessage AS CHARACTER, hasError AS LOGICAL  ):
        
        THIS-OBJECT:errorMessage = errorMessage.
        THIS-OBJECT:hasError = hasError.
        
    END CONSTRUCTOR.
        
        
END CLASS.