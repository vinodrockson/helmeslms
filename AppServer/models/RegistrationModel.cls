 
/*------------------------------------------------------------------------
   File        : RegistrationModel
   Purpose     : To create models for registration
   Syntax      : 
   Description : 
   Author(s)   : vinodrockson
   Created     : Thu Jul 25 03:02:09 EEST 2019
   Notes       : 
 ----------------------------------------------------------------------*/

USING Progress.Lang.*.

BLOCK-LEVEL ON ERROR UNDO, THROW.

CLASS models.RegistrationModel: 

    DEFINE PROPERTY registrationType AS CHARACTER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY renewalTimes AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY renewalInterval AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY lostCoefficient AS DECIMAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY damageCoefficient AS DECIMAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY price AS DECIMAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY fineInterval AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY overdueGp AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY fineGp AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY overdueCoefficient AS DECIMAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY fineCoefficient AS DECIMAL NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY borrowFrequency AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.
    DEFINE PROPERTY borrowLimit AS INTEGER NO-UNDO INITIAL ? GET. 
        PRIVATE SET.

    /*------------------------------------------------------------------------------
     Purpose:
     Notes:
    ------------------------------------------------------------------------------*/
        
    CONSTRUCTOR PUBLIC RegistrationModel ( 
        registrationType AS CHARACTER,
        renewalTimes AS INTEGER,
        renewalInterval AS INTEGER,
        lostCoefficient AS DECIMAL,
        damageCoefficient AS DECIMAL,
        price AS DECIMAL,
        fineInterval AS DECIMAL,
        overdueGp AS INTEGER,
        fineGp AS INTEGER,
        overdueCoefficient AS DECIMAL,
        fineCoefficient AS DECIMAL,
        borrowFrequency AS INTEGER,
        borrowLimit AS INTEGER)
        :
        THIS-OBJECT:registrationType = registrationType.
        THIS-OBJECT:renewalTimes = renewalTimes.
        THIS-OBJECT:renewalInterval = renewalInterval.
        THIS-OBJECT:lostCoefficient = lostCoefficient.
        THIS-OBJECT:damageCoefficient =   damageCoefficient.
        THIS-OBJECT:price = price.
        THIS-OBJECT:fineInterval = fineInterval.
        THIS-OBJECT:overdueGp = overdueGp.
        THIS-OBJECT:fineGp = fineGp.
        THIS-OBJECT:overdueCoefficient = overdueCoefficient.
        THIS-OBJECT:fineCoefficient = fineCoefficient.
        THIS-OBJECT:borrowFrequency = borrowFrequency.
        THIS-OBJECT:borrowLimit = borrowLimit.
        
    END CONSTRUCTOR.


         
    
END CLASS.